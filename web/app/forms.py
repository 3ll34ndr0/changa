from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, IntegerField, TextAreaField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, URL, Length
from app.models import User, Gateway

class LoginForm(FlaskForm):
        username = StringField('Username', validators=[DataRequired()])
        password = PasswordField('Password', validators=[DataRequired()])
        remember_me = BooleanField('Remember Me')
        submit = SubmitField('Sign In')

class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
            'Repeat Password', validators=[DataRequired(),
                EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use different email address.')

class EditProfileForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    about_me = TextAreaField('About me', validators=[Length(min=0, max=140)])
    submit = SubmitField('Submit')

    def __init__(self, original_username, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.original_username = original_username

    def validate_username(self, username):
        if username.data != self.original_username:
            user = User.query.filter_by(username=self.username.data).first()
            if user is not None:
                raise ValidationError('Please use a different username.')

class GatewayForm(FlaskForm):
    name = StringField('Gateway name', validators=[DataRequired()])
    url = StringField('Gateway URL', validators=[DataRequired(), URL()])
    username = StringField('Login username', validators=[DataRequired()])
    password = StringField('Login password', validators=[DataRequired()])
    vendor = StringField('Gateway Vendor')
    alert = BooleanField('Alert if offline')
    submit = SubmitField('Create Gateway')

    def validate_name(self, name):
        gateway = Gateway.query.filter_by(name=name.data).first()
        if gateway is not None:
            raise ValidationError('Please use a different name.')

class EditGatewayForm(FlaskForm):
    name = StringField('Gateway name', validators=[DataRequired()])
    url = StringField('Gateway URL', validators=[DataRequired(), URL()])
    username = StringField('Login username', validators=[DataRequired()])
    password = PasswordField('Login password', validators=[DataRequired()])
    vendor = StringField('Gateway Vendor')
    alert = BooleanField('Alert if offline')
    submit = SubmitField('Edit Gateway')

    def __init__(self, original_name, *args, **kwargs):
        super(EditGatewayForm, self).__init__(*args, **kwargs)
        self.original_name = original_name

    def validate_name(self, name):
        if name.data != self.original_name:
            user = User.query.filter_by(name=self.name.data).first()
            if user is not None:
                raise ValidationError('Please use a differente name.')


class TrunkForm(FlaskForm):
    name = StringField('Trunk name', validators=[DataRequired()])
    maxchans = StringField('Maximum Channels', validators=[DataRequired()])
    dialoutprefix = IntegerField('Outbound Dial Prefix')
    disabled = BooleanField('Disabled')

