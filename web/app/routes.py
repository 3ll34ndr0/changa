from flask import render_template, flash, redirect, request, url_for
from app import app, db
from app.forms import LoginForm, RegistrationForm, EditProfileForm, GatewayForm, EditGatewayForm
from flask_login import current_user, login_user, logout_user, login_required
from app.models import User, Gateway
from werkzeug.urls import url_parse

@app.route('/')
@app.route('/index')
@login_required
def index():
    user = {'username': 'Miguel'}
    posts = [
             {
             'author': {'username': 'John'},
             'body': 'Beautiful day in Portland!'
             },
             {
             'author': {'username': 'Susan'},
             'body': 'The Avengers movie was so cool!'
             }
             ]
    return render_template('index.html', title='ChangaRing', posts=posts)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)

@app.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    posts = [
    {'author': user, 'body': 'Test post #1'},
    {'author': user, 'body': 'Test post #2'}
    ]
    return render_template('user.html', user=user, posts=posts)

@app.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', title='Edit Profile',
                           form=form)


@app.route('/create_gateway', methods=['GET', 'POST'])
@login_required
def create_gateway():
    form = GatewayForm()
    if form.validate_on_submit():
        gateway = Gateway.query.filter_by(name='go').first()
        gateway = Gateway(name=form.name.data, url=form.url.data, username=form.username.data, password=form.password.data, vendor=form.vendor.data, alert=form.alert.data)
        db.session.add(gateway)
        db.session.commit()
        flash('Gateway added!')
        return redirect(url_for('index'))
    return render_template('gateway.html', title='Create Gateway', form=form)

@app.route('/gateways', methods=['GET'])
@login_required
def gateways():
    gateways = Gateway.query.all()
    return render_template('gateways.html', title='Gateways', gateways=gateways)

@app.route('/edit_gateway/<name>', methods=['GET', 'POST'])
@login_required
def edit_gateway(name):
    form = EditGatewayForm(name)
    gateway = Gateway.query.filter_by(name=name).first()
    if form.validate_on_submit():
        gateway.name = form.name.data
        gateway.url = form.url.data
        gateway.username = form.username.data
        gateway.password = form.password.data
        gateway.vendor = form.vendor.data
        gateway.alert = form.alert.data
        db.session.add(gateway)
        db.session.commit()
        flash('Your changes have been saved!')
        return redirect(url_for('index'))
    elif request.method == 'GET':
        form.name.data = gateway.name
        form.url.data = gateway.url
        form.username.data = gateway.username
        form.password.data = gateway.password
        form.vendor.data = gateway.vendor
        form.alert.data = gateway.alert
    return render_template('edit_gateway.html', title='Edit Gateway {}'.format(gateway.name), form=form)

