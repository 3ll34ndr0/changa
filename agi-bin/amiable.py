#!/usr/bin/env python3

from time import sleep
from datetime import datetime
from sys import argv
from email.mime.text import MIMEText
from subprocess import Popen, PIPE, run
# Lo meto aca para empezar pero despues tiene que estar a parte:
import redis

try:
    import pymysql as MySQLdb
    MySQLdb.install_as_MySQLdb()
except ImportError:
    # MySQLdb is compatible with Python 2 only.  Try it as a
    # fallback if pymysql is unavailable.
    import MySQLdb


log_file = open('/var/log/asterisk/changarq.log', 'a', buffering=1)
def log(*args):
    print(datetime.now().isoformat()[:19], *args, file=log_file)
#    print(datetime.now().isoformat()[:19], *args)
#log2_file = open('/var/log/asterisk/solouna.log', 'a', buffering=1)
#def log2(*args):
#    print(datetime.now().isoformat()[:19], *args, file=log2_file)
#    print(datetime.now().isoformat()[:19], *args)



def amiable(hostdb,
               userdb, 
               passdb, 
               db,
               uniqueid, 
               extension,
               ):
   # Get all data from where it was called
   log("Checking if there is any trunk available....")
   cnx = MySQLdb.connect(user=userdb,passwd=passdb,host=hostdb,db=db)
   cursor = cnx.cursor()
#   cursor.execute("SELECT appdata FROM cel WHERE appname='MixMonitor' AND uniqueid="+uniqueid+"LIMIT 1" )
   cursor.execute("SELECT COUNT(disabled) FROM trunks WHERE disabled='off'" )

   trunksAvailable= cursor.fetchall()[0][0]
   log("Found "+str(trunksAvailable)+" trunks available.")
   cursor.close()
   cnx.close()
   return trunksAvailable

if __name__ == '__main__':
   #Useful to debug
   hostdb, userdb, passdb, db, uniqueid, =  argv[1:]
   print(amiable( hostdb, userdb, passdb, db, uniqueid))
