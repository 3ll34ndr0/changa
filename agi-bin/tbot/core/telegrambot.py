import asyncio
import logging
import urllib3
from functools import wraps
from re import match
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ParseMode
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, \
            ConversationHandler, MessageHandler, BaseFilter, run_async, Filters
from subprocess import  PIPE, run
import datetime
from os import remove
import emoji
from util import redis
from util.goip import Goip
from config.gateway import GATEWAY
from config.settings import billing_prefix, admins, TRUNKNAME, MAXTRUNKS
from util.whitelist import load_whitelist
from redis import Redis
r = Redis(host='localhost')
p = r.pubsub()
p.subscribe('alerts')

gateways = {}
for gw in GATEWAY.keys():
    url = GATEWAY[gw]['url']
    user = GATEWAY[gw]['user']
    password = GATEWAY[gw]['password']
    gateways[gw] = Goip(url, gw, user=user, password=password)
REDIS_GATEWAY = redis.connect_gateways()
# This will be in the ddbb:
due='True'
solouna       = '/var/lib/asterisk/agi-bin/solouna.py' 
mirablanquito = '/var/lib/asterisk/agi-bin/mirablanquito.sh' 
whitelistFile = '/etc/asterisk/blanquito.txt'
# Log configuration
log_file = '/var/log/asterisk/telegrambot.log'
logging.basicConfig(filename=log_file, level=logging.DEBUG)

CONFIRM = "confirm"
CANCEL = "cancel"
ENABLE = "enable"
DISABLE = "disable"
STATUS = "status"
END_CONVERSATION = ConversationHandler.END

WHITE_LIST = "whitelist"
WHITELIST_OPTION = "whitelist_option"
WHITELIST_NEW = "whitelist_new"
WHITELIST_UPLOAD = "whitelist_upload"
WHITELIST_DOWNLOAD = "whitelist_download"

PAY_OPTION  = "pay_option"
PAYTRUNKS16 = "paytrunks16"
PAYTRUNKS32 = "paytrunks32"
PAYTRUNKS48 = "paytrunks48"
PAYTRUNKS64 = "paytrunks64"


BOUNCE_FILTER = "bounce_filter"
BOUNCEFILTER_OPTION = "bouncefilter_option"

TRUNKS = "trunks"
CDR = "cdr"
STATISTICS = "statistics"
PAY = "pay"
ENABLE_DISABLE_OPTION  = "enable_disable_option"
ACTIVE_CALLS = "active_calls"
ADMIN_SELECT = "admin_select"


# Esto es porque estuve renegando mucho parameterlocomo modulo,
# rápido y sucio lo hago aca
try:
    import pymysql as MySQLdb
    MySQLdb.install_as_MySQLdb()
except ImportError:
    # MySQLdb is compatible with Python 2 only.  Try it as a
    # fallback if pymysql is unavailable.
    import MySQLdb
from os import path
c_dir = path.dirname(__file__)
with open(path.join(c_dir, "../config/asterisksecrets.txt")) as key_file:
   hostdb, userdb, passdb, dbcdr, dbasterisk, monitor_dir = key_file.read().splitlines()


class TelegramBot:
    class PrivateUserFiler(BaseFilter):
        def __init__(self, user_id ):
            self.users_id = user_id

        def filter(self, message):
            print(message)
            print(message.from_user)
#            return message.from_chat.id == self.chat_id
            return self.users_id.count(str(message.from_user.id)) == 1
    #chat_ids: Channels where to publish data
    def __init__(self, token: str, allowed_user_id , chat_ids, ivr): 
        self.updater = Updater(token=token )
        self.dispatcher = self.updater.dispatcher
        self.private_filter = self.PrivateUserFiler(allowed_user_id )
        self.jq    = self.updater.job_queue
        self._prepare(ivr, chat_ids)
        self.chat_ids = chat_ids

    def _send_all_channels(self, bot, message, chat_ids=None):
        """ Send message to all subscribed channels """
        if chat_ids is None:
            chat_ids = self.chat_ids.split()
        for chat_id in chat_ids:
            try:
                bot.send_message(chat_id=chat_id, text=message, parse_mode=ParseMode.MARKDOWN)
            except Exception as e:
                print(e)
                pass


    def _prepare(self, ivr, chat_ids):
        """ Do some tasks at the start up """
        # Create our handlers
        def callback_due_date(bot, update):
           keyboard = [
                      [InlineKeyboardButton("16 Trunks: $180 usd", callback_data=PAYTRUNKS16, url='https://www.paypal.me/changaring/180'),],
                      [InlineKeyboardButton("32 Trunks: $320 usd", callback_data=PAYTRUNKS32, url='https://www.paypal.me/changaring/320'),],
                      [InlineKeyboardButton("48 Trunks: $432 usd", callback_data=PAYTRUNKS48, url='https://www.paypal.me/changaring/432'),],
                      [InlineKeyboardButton("64 Trunks: $512 usd", callback_data=PAYTRUNKS64, url='https://www.paypal.me/changaring/512'),],
                      ]

#               bot.edit_message_text(text="You are on due-date. Renew subscription to avoid suspension.",
           bot.edit_message_text(text="Su suscripción vencerá el 16 de mayo a las 23:59 horas. Renueve su servicio y evite que sea suspendido. Elija su suscripción mensual:",
                                 chat_id=query.message.chat_id,
                                 message_id=query.message.message_id,
                                 reply_markup=InlineKeyboardMarkup(keyboard))

        def callback_alerts(bot, update):
            """ Will fetch from redis publish/subscribe channels and send message to admins """
            message = p.get_message()['data'].decode('utf-8')
#            if message and not message == '1':
#                self._send_all_channels(bot, message, chat_ids=admins)

        def callback_calls_status(bot, update):
            channel_re = r'SIP/([a-zA-Z0-9]+)-[a-f0-9]+'
            fields_re = channel_re + ' .+ (\d*) .+(\d\d:\d\d:\d\d) ([a-zA-Z0-9_]+)\s*([a-zA-Z0-9_]*).*'
            active_re = r'(\d+) active calls'
            calls = dict()
            originators = set()
            statusOut = run(['/usr/sbin/asterisk', '-rx core show channels verbose'], stdout=PIPE).stdout.decode('utf-8')
            for lin in statusOut.split('\n'):
                matched = match(fields_re, lin)
                matched_active = match(active_re, lin)
                if matched:
                    channel, dialed_number, duration, account_code, peer_account = matched.groups()
                    if not dialed_number is '':
                        trunkname = channel
                        try:
                            r.sadd('peers_account', peer_account)
                            originators.add(peer_account)
                            calls[peer_account].append((trunkname, dialed_number,  duration))
                            print(originators)
                        except KeyError as e:
                            print(e)
                            calls[peer_account]=[(trunkname, dialed_number,  duration)]
                        # It is the first entry, so I create a new key and value for this originator
                elif matched_active:
                    current_calls_global = matched_active.groups()[0]
                    r.set('current_calls_global', current_calls_global)
            for account in calls.keys():
                r.set('current_calls_' + peer_account, len(calls[account]))
            historic_peers = [peer.decode('utf-8') for peer in (r.smembers('peers_account'))]
            
            # Need to reset the counter when no calls where found
            for peer in historic_peers:
                if not peer in originators:
                    r.set('current_calls_' + peer, 0)

        def callback_gateway(bot, update):
            message = ''
            for gw in gateways:
                gw_counter = 0
                logging.info('Updating gateway {}'.format(gw))
                g = gateways[gw]
                try:
                    g.update()
                except urllib3.exceptions.MaxRetryError as max_retry:
                    logging.error(max_retry)
#                    message += 'ERROR: {} is offline\n'.format(gw)
#                    message += emoji.emojize(':warning: {} is offline\n'.format(gw))
                    continue
                rgc = REDIS_GATEWAY
                for ch in g.channels:
                    name = 'gw-{}-{}'.format(gw, str(ch))
                    durname = 'duration-' + name
                    durname_accum = 'accum-' + durname
                    countname = 'counter-' + durname
                    duration = int(g.channels[ch]['Duration'].replace('s','').replace('m',''))
                    if (rgc.setnx(durname, duration)):
                        rgc.expire(durname, 30000)
                        rgc.set(countname, 0)
                        message += 'Counter for line {} is now 0.\n'.format(name)
                    else:
                        old_duration = int(rgc.get(durname))
                        rgc.set(durname, duration)
                        if old_duration - duration > 0:
                            rgc.incr(countname)
                            if (rgc.setnx(durname_accum, old_duration)):
                                # One week
                                rgc.expire(durname, 604800)
                            else:
                                rgc.incrby(durname_accum, old_duration) 
                                
                            message += 'Duration was set from {} to {}. '\
                                       'Counter for line {} is now {}. '\
                                       'Accumulated duration: {}\n'.format(old_duration,
                                                                           duration,
                                                                           name,
                                                                           rgc.get(countname).decode('UTF-8'),
                                                                           rgc.get(durname_accum).decode('UTF-8'))
            if message is not '':

                for gw in gateways:
                    message += '`{}`: *{}* sims\n'.format(gw, redis.sum_all_values('counter-duration-gw-' + gw + '*'))
                self._send_all_channels(bot, message, chat_ids=admins)

        def callback_rotate_trunks(bot, update):
            ###Will try something new: trunks rotation:
            cnx_rotation = MySQLdb.connect(user=userdb,passwd=passdb,host=hostdb,db=dbasterisk)
            cursor_rotation = cnx_rotation.cursor()
            # uniqueid, trunkname, filename:
            # TODO: Hacerlo mas general
            sqlCommand = '''SELECT DISTINCT route_id FROM outbound_route_trunks'''
            cursor_rotation.execute(sqlCommand) 
            outbound_routes= cursor_rotation.fetchall()
            for outbound_route in outbound_routes:
                sqlCommand = '''SELECT * FROM outbound_route_trunks WHERE route_id={}'''.format(outbound_route[0])
                cursor_rotation.execute(sqlCommand) 
                trunk_sequence = cursor_rotation.fetchall()
                print(trunk_sequence)
                #  print(list(zip(*trunk_sequence)))
                route_id, trunk_id, seq = list(zip(*trunk_sequence))
                #   print(seq)
                seq = [x-1 for x in  seq]
                seq[seq.index(min(seq))] = max(seq)+1
                #   print(seq)
                #  print(list(zip(*trunk_sequence)))
                trunk_sequence=list(zip(route_id, trunk_id, seq))
                for route_id, trunk_id, seq in trunk_sequence:
                    sqlCommand = "UPDATE outbound_route_trunks SET seq='{}' WHERE trunk_id='{}' AND route_id='{}'".format(seq, trunk_id, route_id)
                    cursor_rotation.execute(sqlCommand) 
            cursor_rotation.execute("COMMIT") 
            cursor_rotation.close()
            cnx_rotation.close()
            log('Now we are reloading asterisk ...')
            log(run(['/var/lib/asterisk/bin/module_admin', 'reload'], stdout=PIPE).stdout.decode('utf-8'))
            print(trunk_sequence)
            #   needReloadAsterisk = True


        def callback_limit(bot, update):
           #seguir
           trunksEnabled=list()
           needReloadAsterisk = False

           for trunk,disabled in getTrunksName():
#              print(trunk,disabled)
              if( match('^{}.+'.format(TRUNKNAME),trunk) and (disabled=="off")):
                trunksEnabled.append(trunk)
              else:
                pass
           print(trunksEnabled, len(trunksEnabled))
            
           while( len(trunksEnabled) - MAXTRUNKS > 0 ):
              disableTrunk       = trunksEnabled.pop()
              needReloadAsterisk = True
              try:
                 print("Debería desabilitar este trunk: {}".format(disableTrunk))
                 cnx2    = MySQLdb.connect(user=userdb,passwd=passdb,host=hostdb,db=dbasterisk)
                 cursor2 = cnx2.cursor()
                 cursor2.execute("UPDATE trunks set disabled='on' WHERE name='{}'".format(disableTrunk)) 
              except:
                 needReloadAsterisk = False
                 loggin.error("{}: Falló al intentar desabilitar el canal {}".format(e,disableTrunk))
                 loggin.error("UPDATE trunks set disabled='on' WHERE name='{}'".format(disableTrunk))
                 cursor2.close()
                 cnx2.close()

           if(needReloadAsterisk):
              #asterisk
              cursor2.execute("COMMIT") 
              cursor2.close()
              cnx2.close()
              log('Now we are reloading asterisk ...')
              log(run(['/var/lib/asterisk/bin/module_admin', 'reload'], stdout=PIPE).stdout.decode('utf-8'))


        def callback_audio_analysis(bot, update):
#             query = update.callback_query
           print('ivr')
           messageIVR=ivr()
           print(messageIVR)
           logging.info(messageIVR)

           if messageIVR!="":
             bot.send_message(chat_id='247422441', text=messageIVR) #That's my id
             for chat_channel in chat_ids.split():
                bot.send_message(chat_id=chat_channel,
                              text=messageIVR)


        def callback_weekly_invoice(bot, update):
            logging.info('Weekly invoice summary creation')
            output_message = '* Weekly Invoice Summary *\n'
            output_message += '{:15s} {}\n'.format('Account', 'Billed minutes')
#            sql_command = """SELECT peeraccount, SUM(billsec)/60 FROM cdr WHERE billsec >0 AND (calldate > (now() - INTERVAL 7 DAY)) GROUP BY peeraccount"""
            sql_command = """SELECT peeraccount, SUM(billsec)/60 FROM cdr 
                             WHERE billsec >0  
                             AND dst LIKE '{}%' 
                             AND (calldate > (now() - INTERVAL 7 DAY - INTERVAL 1 HOUR)) 
                             AND (calldate < (now() - INTERVAL 1 HOUR)) 
                             group by peeraccount"""
            try:
                cnx2 = MySQLdb.connect(user=userdb,passwd=passdb,host=hostdb,db=dbcdr)
                cursor2 = cnx2.cursor()
                for prefix in billing_prefix:
                    cursor2.execute(sql_command.format(prefix)) 
                    for account, minutes in cursor2.fetchall():
                        if account == "":
                            account = 'Unbilled'
                        output_message += '{:21s} {:<.2f}\n'.format(account+':', minutes)
                        print(output_message)
                        logging.info(output_message)
                cursor2.close()
                cnx2.close()
                print(output_message)
                # DEBUG and DEV
#                chat_ids='-284929703'
                for chat_channel in chat_ids.split():
                    bot.send_message(chat_id=chat_channel,
                                     text=output_message)
            except:
                loggin.error("Falló al intentar hacer lo siguiente:\n{}\n".format(sql_command))
                cursor2.close()
                cnx2.close()
        today = datetime.date.today()
        d = today + datetime.timedelta(days=-today.weekday(), weeks=1 )
        # Monday first thing in the 0 AM (morning)
        billing_date = datetime.datetime(d.year, d.month, d.day) + datetime.timedelta(hours=1)
#        job_weekly_invoice = self.jq.run_repeating(callback_weekly_invoice, 
#                                                   interval=60,
#                                                   first=1)
        job_weekly_invoice = self.jq.run_repeating(callback_weekly_invoice, 
                                                   interval=datetime.timedelta(weeks=1), 
                                                   first=billing_date)

        job_rotate_outbound_routes = self.jq.run_repeating(callback_rotate_trunks, interval=7)

        job_gateway = self.jq.run_repeating(callback_gateway, interval=165, first=3)
        job_calls_status = self.jq.run_repeating(callback_calls_status, interval=3)
        job_minute = self.jq.run_repeating(callback_audio_analysis, interval=12, first=5) # Should uncomment before merge

        job_alerts = self.jq.run_repeating(callback_alerts, interval=5)
#        job_limit  = self.jq.run_repeating(callback_limit, interval=11, first=3)


# Manual job for puting was the goose message:
#        job_due_date = self.jq.run_repeating(callback_due_date, interval=43200, first=1200) #Twice a day


        def show_help(bot, update):
            update.effective_message.reply_text('Type /admin to show options ')

        def debugCall(bot, update):
#            textFromUser = update.message.from_user #Lost of internal data
            textFromUser = update.message.text.split()[1]
#            query = update.callback_query
            outputText   = ""
#            update.effective_message.reply_text("Invalid uniqueid {}".format(textFromUser))

            # Check if we have a valid uniqueid:
            if( match('^\d.+\.\d.+$',textFromUser) ):                          
               sqlcommand = "select calldate,recordingfile,disposition,duration, dstchannel from cdr where uniqueid='{}'".format(textFromUser)
               cnx = MySQLdb.connect(user=userdb,passwd=passdb,host=hostdb,db=dbcdr)
               cursor = cnx.cursor()
               cursor.execute(sqlcommand)
               cdrQueryResult = cursor.fetchall()
               cursor.close()
               cnx.close()
               for date, audio, disposition, duration, dstchannel in cdrQueryResult:
                   print(date, audio, disposition, duration, dstchannel)
                   outputText += "{} {} {}s {}\n".format(date, disposition, duration, dstchannel.split("-")[0])

               update.effective_message.reply_text(outputText)
               fullPathAudio = findAudio(audio, monitor_dir)
               bot.send_voice(chat_id=update.message.chat_id, voice=open(fullPathAudio, 'rb'))




            else:
                update.effective_message.reply_text("Invalid uniqueid {}".format(textFromUser))



        def show_options(bot, update):
            button_list = [
                    # First row
                    [InlineKeyboardButton(emoji.emojize(':open_book: Whitelist'), callback_data=WHITE_LIST),
                     InlineKeyboardButton(emoji.emojize(':triangular_flag: Bounce Filter'), callback_data=BOUNCE_FILTER), ],
#                 InlineKeyboardButton("Status", callback_data=STATUS)],
                    # Second row
                    [InlineKeyboardButton(emoji.emojize(':tram: Trunks'), callback_data=TRUNKS),
                     InlineKeyboardButton(emoji.emojize(':telephone_receiver:  Active calls'), callback_data=ACTIVE_CALLS)],
                    # Third row
                    [InlineKeyboardButton(emoji.emojize(':receipt: CDR'), callback_data=CDR),
                        InlineKeyboardButton(emoji.emojize(':selfie: Statistics'), callback_data=STATISTICS)                              ],
#                     InlineKeyboardButton(emoji.emojize(':warning: Suscripción Vencida'), callback_data=PAY)                              ],
            ]
            update.message.reply_text("Admin options:", reply_markup=InlineKeyboardMarkup(button_list))
            return ADMIN_SELECT


        def process_admin_selection(bot, update, user_data):
            query = update.callback_query
            selection = query.data

            if selection == CDR:
#               try: 
               cnx3 = MySQLdb.connect(user=userdb,passwd=passdb,host=hostdb,db=dbcdr)
               cursor3 = cnx3.cursor()
               cdrFile = "/tmp/last_30_days_{}.csv".format(datetime.date.today())
               fieldsToSelect = "calldate,clid,src,dst,billsec,disposition,accountcode"
               sqlcommand = '''select {} INTO OUTFILE '{}' FIELDS TERMINATED BY ','  OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n' from cdr where billsec>0 and calldate > (NOW() - INTERVAL 30 day) ORDER BY accountcode,calldate'''.format(fieldsToSelect, cdrFile)
               cursor3.execute(sqlcommand)
               cursor3.close()
               cnx3.close()
               bot.send_document(chat_id=query.message.chat_id, document=open(cdrFile, 'rb'))
               remove(cdrFile)
#               except:
#                  log("{}: Falló al intentar crear un CDR"
#                  cursor3.close()
#                  cnx3.close()
#
               return END_CONVERSATION

            if selection == STATISTICS:
               #TODO: Un hardcode it
               minutes = 60
               acd_template = 'Your current ACD is {0:.2f} mins, with {1} calls in the last {2} minutes\n'
               asr_template = 'Your current ASR is %{0:.2f}, with {1} seized calls in the last {2} minutes\n'
               traffic_template = 'Your traffic was {} in the last {} minutes'
               cnx3 = MySQLdb.connect(user=userdb,passwd=passdb,host=hostdb,db=dbcdr)
               cursor3 = cnx3.cursor()
               sqlcommand = """select count(billsec),avg(billsec)/60 from cdr where billsec>0 and calldate > (NOW() - INTERVAL {} minute)""".format(minutes)
               cursor3.execute(sqlcommand)
               print(cursor3.rowcount)
               calls,acd = cursor3.fetchall()[0]
               print(acd, calls)
               try:
                   message = acd_template.format(acd,calls, minutes)
                   sqlcommand = '''select count(*) from cdr where calldate > (NOW() - INTERVAL {} minute)'''.format(minutes)
                   cursor3.execute(sqlcommand)
                   seized_calls = cursor3.fetchall()[0][0]
                   print(seized_calls, type(seized_calls))
                   asr = 100.0 * calls/seized_calls
                   message += asr_template.format(asr, seized_calls, minutes)
                   message += traffic_template.format(int(acd*minutes), minutes)
               except TypeError:
                   message= 'No successful calls in the last {} minutes.'.format(minutes)
               message += '\n________________________________\n'
               for gw in gateways:
                   durname_accum = 'accum-duration-gw-{}*'.format(gw)
                   mins = redis.sum_all_values(durname_accum)
                   sims = redis.sum_all_values('counter-duration-gw-' + gw + '*')
                   try: 
                       rate = int(mins/sims)
                   except:
                       continue
                   message += '\n{} accum mins/sims: {}/{}: {}'.format(gw,mins, sims, str(rate) + ' min/sim')

               bot.edit_message_text(text=message,
                                         chat_id=query.message.chat_id,
                                         message_id=query.message.message_id)
               cursor3.close()
               cnx3.close()
               return END_CONVERSATION




            if selection == PAY:
               keyboard = [
                       [InlineKeyboardButton("16 Trunks: $180 usd", callback_data=PAYTRUNKS16, url='https://www.paypal.me/changaring/180'),],
                       [InlineKeyboardButton("32 Trunks: $320 usd", callback_data=PAYTRUNKS32, url='https://www.paypal.me/changaring/320'),],
                       [InlineKeyboardButton("48 Trunks: $432 usd", callback_data=PAYTRUNKS48, url='https://www.paypal.me/changaring/432'),],
                       [InlineKeyboardButton("64 Trunks: $512 usd", callback_data=PAYTRUNKS64, url='https://www.paypal.me/changaring/512'),],
               ]

#               bot.edit_message_text(text="You are on due-date. Renew subscription to avoid suspension.",
               bot.edit_message_text(text="Su suscripción está vencida. Renueve su servicio y evite que sea suspendido. Elija su suscripción mensual:",
                                      chat_id=query.message.chat_id,
                                      message_id=query.message.message_id,
                                      reply_markup=InlineKeyboardMarkup(keyboard))

               return PAY_OPTION 

               


            if selection == WHITE_LIST:

                # Ask what to do
                keyboard = [
                    [InlineKeyboardButton("Disable", callback_data=DISABLE),],
                    [InlineKeyboardButton("Enable", callback_data=ENABLE),],
                    [InlineKeyboardButton("Upload new whitelist", callback_data=WHITELIST_NEW),],
                    [InlineKeyboardButton("Download current whitelist", callback_data=WHITELIST_DOWNLOAD),],
                ]

                bot.edit_message_text(text="Choose your option: ",
                                      chat_id=query.message.chat_id,
                                      message_id=query.message.message_id,
                                      reply_markup=InlineKeyboardMarkup(keyboard))

                # attach opened orders, so that we can cancel by index
                return WHITELIST_OPTION 

            elif selection == STATUS:
                    bot.edit_message_text(text="Not implemented yet",
                                          chat_id=query.message.chat_id,
                                          message_id=query.message.message_id)
                    return END_CONVERSATION


            elif selection == ACTIVE_CALLS:
                    calls = dict()
                    originators = set()
                    statusOut = run(['/usr/sbin/asterisk', '-rx core show channels verbose'], stdout=PIPE).stdout.decode('utf-8')
                    salida="Trunk - Number - Duration\n"
                    for lin in statusOut.split('\n'):
                       col = lin.split()
                       try:
#                          print(trunkname)
                          #SIP/GOIP27-0018a704  macro-dialout-trunk  s 1 Up (Outgoing Line) 206166593741851 00:19:53 cliev cliev b71-60-a-8
                          if( match('^SIP/{}.*'.format(TRUNKNAME),lin) and (col[3] == 'Up')):                          
                             #SIP/GOIP18-0018adae #Get the GOIP18
                             trunkname = col[0].split('/')[1].split('-')[0]
                             #accountCode
                             originator = col[9]
                             originators.add(originator) # Create a set with originators
                             # 206166593741851 # strip the 206166
                             dialedNumber = col[7][:]
                             print(dialedNumber)
                             duration = col[8]
#                             salida = salida+"{} {} {} {}\n".format(originator, trunkname, dialedNumber, col[9]) #col[8] Duration 
                             try:
                                calls[originator].append("{} {} {}".format(trunkname, dialedNumber,  duration))
                             except KeyError as e:
                                # It is the first entry, so I create a new key and value for this originator
                                calls[originator]=["{} {} {}".format(trunkname, dialedNumber,  duration)]
                       except IndexError as e:
                             print(e)
                       #      salida = salida+"{}   {}\n".format(col[0], col[6])
                             print(col)
                            # "Online trunks: 0"
#                    salida += statusOut.split('\n')[-3]+'\n'
#                    salida += statusOut.split('\n')[-2]
                    print(calls)
                    for originator in calls.keys():
                        salida += 'From {} {} calls:\n'.format(originator, len(calls[originator]))
                        for call in calls[originator]:
                            print(call)
                            salida += '{}\n'.format(call)
                        salida +='________________________________\n'
                    salida += statusOut.split('\n')[-3]+'\n'
                    salida += statusOut.split('\n')[-2]




                    bot.edit_message_text(text=salida,
                                         chat_id=query.message.chat_id,
                                         message_id=query.message.message_id)
                    return END_CONVERSATION


            elif selection == TRUNKS:
#                 def trunks(bot, update, user_data):
                    statusOut = run(['/usr/sbin/asterisk', '-rx sip show peers like '+TRUNKNAME], stdout=PIPE).stdout.decode('utf-8')
                    salida="Name/username   Status\n"
                    for lin in statusOut.split('\n'):
                       if( match('^{}.+'.format(TRUNKNAME),lin) ):
                          col = lin.split()
                          try:
                              salida = salida+"{} {} {} {}\n".format(col[0], col[6], col[7], col[8])
                          except IndexError as e:
                              print(e)
                              print(col)
                              if(col[2]=='D'):
                                 salida = salida+"{}   {}\n".format(col[0], col[6])
                              else:
                                 salida = salida+"{}   {}\n".format(col[0], col[5])    
                             # "Online trunks: 0"

                    bot.edit_message_text(text=salida,
                                         chat_id=query.message.chat_id,
                                         message_id=query.message.message_id)
                    return END_CONVERSATION


#            elif selection == ACTIVE_CALLS:
#                    bot.edit_message_text(text="Not implemented yet",
#                                          chat_id=query.message.chat_id,
#                                          message_id=query.message.message_id)
#                    return END_CONVERSATION


            elif selection == BOUNCE_FILTER:
                # Ask what to do
                keyboard = [
                    [InlineKeyboardButton("Disable", callback_data=DISABLE),],
                    [InlineKeyboardButton("Enable", callback_data=ENABLE),],
#                    [InlineKeyboardButton("Set hoursUpload new whitelist", callback_data=WHITELIST_NEW),],
#                    [InlineKeyboardButton("Download current whitelist", callback_data=WHITELIST_DOWNLOAD),],
                ]

                bot.edit_message_text(text="What do you want to do with the Bounce Filter?: ",
                                      chat_id=query.message.chat_id,
                                      message_id=query.message.message_id,
                                      reply_markup=InlineKeyboardMarkup(keyboard))

                # attach opened orders, so that we can cancel by index
                return BOUNCEFILTER_OPTION 



#        def send_typing_action(func):
#           """Sends typing action while processing func command."""
#
#           @wraps(func)
#           def command_func(update, context, *args, **kwargs):
#              context.bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.TYPING)
#              return func(update, context,  *args, **kwargs)
#
#           return command_func

        def pay_option(bot, update):
            query = update.callback_query
            selection = query.data
            if selection == PAYTRUNKS16:
                bot.edit_message_text(text='Noseai raton, pagá un poco mas',
                                          chat_id=query.message.chat_id,
                                          message_id=query.message.message_id)
                pass
            elif selection == PAYTRUNKS32:
                pass
            elif selection == PAYTRUNKS48:
                pass
            elif selection == PAYTRUNKS64:
                pass
            return END_CONVERSATION

#        @send_typing_action
        def whitelist_option(bot, update):
            from os import chown
            from shutil import copyfile
            query = update.callback_query
            selection = query.data
            print(selection)
            if selection == DISABLE:
#                query.message.reply_text('You disabled whitelist!')
                try:
                    copyfile(mirablanquito+'.disabled', mirablanquito)
                    bot.edit_message_text(text='You disabled whitelist!',
                                          chat_id=query.message.chat_id,
                                          message_id=query.message.message_id)
                except:
                    bot.edit_message_text(text='An error ocurred on disabling whitelist!',
                                          chat_id=query.message.chat_id,
                                          message_id=query.message.message_id)

            elif selection == ENABLE:
                try:
                    copyfile(mirablanquito+'.enabled', mirablanquito)
                    bot.edit_message_text(text='Your whitelist is up now!',
                                          chat_id=query.message.chat_id,
                                          message_id=query.message.message_id)
                except:
                    bot.edit_message_text(text='An error ocurred while enabling whitelist!',
                                          chat_id=query.message.chat_id,
                                          message_id=query.message.message_id)
            elif selection  == WHITELIST_NEW:
                bot.edit_message_text(text='Please send me a whitelist file. The format must be one number per line only.',
                                          chat_id=query.message.chat_id,
                                          message_id=query.message.message_id)
                return WHITELIST_UPLOAD
#                file_id=update.message.document.file_id
            elif query.data == WHITELIST_DOWNLOAD:
               bot.send_document(chat_id=query.message.chat_id, document=open(whitelistFile, 'rb'))
               return END_CONVERSATION

            return END_CONVERSATION


##############
        def trunkss_option(bot, update):
#            from os import chown
#            from shutil import copyfile
            query = update.callback_query
            selection = query.data
            print(selection)
            if selection == DISABLE:
                query.message.reply_text('WIP!')

            elif selection == ENABLE:
                #salida =""
                keyboard = list()
                for trunk,disabled in getTrunksName():
                   user_data[trunk]=disabled
                   if disabled=="on":
                       print("{} {}".format(trunk,disabled))
#                       salida += "Enable {}\n".format(trunk)
#                       keyboard.append([InlineKeyboardButton("Enable {}\n".format(trunk),callback_data="{} {}".format(ENABLE, trunk))])
                       keyboard.append([InlineKeyboardButton("▶️  Enable {}".format(trunk),callback_data=trunk)])
                       bot.edit_message_text(text="Choose your option: ",
                                      chat_id=query.message.chat_id,
                                      message_id=query.message.message_id,
                                      reply_markup=InlineKeyboardMarkup(keyboard))

                return ENABLE_DISABLE_OPTION 

            elif selection  == STATUS:
                salida = getTrunksStatus()
                query.message.reply_text(salida)
                return END_CONVERSATION 




##############
        def enable_disable_option(bot, update, user_data):
            query = update.callback_query
            option = query.data
#            option,trunk = selection
            if option== DISABLE:
                query.message.reply_text('WIP!')

            elif option == ENABLE:
                #salida =""
                keyboard = list()
                for trunk,disabled in getTrunksName():
                   if disabled=="on":
                       print("{} {}".format(trunk,disabled))
#                       salida += "Enable {}\n".format(trunk)
#                       keyboard.append([InlineKeyboardButton("Enable {}\n".format(trunk),callback_data="{} {}".format(ENABLE, trunk))])
                       keyboard.append([InlineKeyboardButton("▶️  Enable {}".format(trunk),callback_data=ENABLE)])
                   else:
                       print("{} {}".format(trunk,disabled))
#                       salida += "Disable {}\n".format(trunk)
                      # keyboard.append(list(InlineKeyboardButton("Disable {}\n".format(trunk),callback_data="{} {}".format(DISABLE, trunk))))
                       keyboard.append([InlineKeyboardButton("⏹️  Disable {} ".format(trunk),callback_data=DISABLE)])
                       bot.edit_message_text(text="Choose your option: ",
                                      chat_id=query.message.chat_id,
                                      message_id=query.message.message_id,
                                      reply_markup=InlineKeyboardMarkup(keyboard))

                return ENABLE_DISABLE_OPTION 







        def whitelist_upload(bot, update, user_data):
            print(dir(update.message))
            print(dir(update.message.document))
            query = update.callback_query
            file_id = update.message.document.file_id
            print('entro')
            file = bot.getFile(file_id)
            print("Nombre de archivo: {}".format(update.message.document.file_name))
            tempfilename = 'whitelist.temporal'
            file.download(tempfilename)
            fileFormat = ''
            # Testing redis whitelist
            print('entraaa')
            load_whitelist(tempfilename)
            print('saleeee') 
            # Bash whitelist
            with open(tempfilename, "r") as fil:
#               while True:
                  # Will only check first line:
                  num=fil.readline().strip("\n")
                  # Check format: Only one number per line. No spaces or non digit characters.
                  if( not match('^\d+$',num) ):
                     bot.send_message(chat_id=update.message.chat_id, text='Wrong format! File should include only one telephone number by line. No spaces or non digit characters.')
                     fileFormat = 'error'
#                     break

            if( fileFormat != 'error' ):
               from shutil import copyfile
               from os import chown
               from pwd import getpwnam
               from grp import getgrnam  
               uid = getpwnam('asterisk').pw_uid
               gid = getgrnam('asterisk').gr_gid
               try:
                  copyfile(tempfilename, whitelistFile)
                  chown( whitelistFile, uid, gid)
                  bot.send_message(chat_id=update.message.chat_id, text='Whitelist has been uploaded correctly.')
               except:
                  bot.send_message(chat_id=update.message.chat_id, text='Error while trying to upload whitelist.')

            return END_CONVERSATION


        def bouncefilter_option(bot, update ):
            query = update.callback_query
            selection = query.data
            from shutil import copyfile
            print(selection)
            if selection == DISABLE:
                try:
                    copyfile(solouna+'.disabled', solouna)
                    bot.edit_message_text(text='Bounce Filter disabled!',
                                          chat_id=query.message.chat_id,
                                          message_id=query.message.message_id)
                except:
                    print(er)
                    bot.edit_message_text(text='An error ocurred on disabling Bounce Filter!',
                                          chat_id=query.message.chat_id,
                                          message_id=query.message.message_id)

            elif selection == ENABLE:
                try:
                    copyfile(solouna+'.enabled', solouna)
                    bot.edit_message_text(text='Done!',
                                          chat_id=query.message.chat_id,
                                          message_id=query.message.message_id)
                except:
                    print(er)
                    bot.edit_message_text(text='An error ocurred while enabling Bounce Filter!',
                                          chat_id=query.message.chat_id,
                                          message_id=query.message.message_id)


#        @send_typing_action
#        def my_handler(bot, update):
#                pass # Will send 'typing' action while processing the request.


        def handle_error(bot, update, error):
            logging.warning('Update "%s" caused error "%s"', update, error)
            update.message.reply_text('Unexpected error:\n{error}'.format(error))

        # configure our handlers
        def build_conversation_handler():
            entry_handler = CommandHandler('admin', filters=self.private_filter, callback=show_options)
            conversation_handler = ConversationHandler(
                entry_points=[entry_handler],
                fallbacks=[entry_handler],
                states={
                    ADMIN_SELECT: [CallbackQueryHandler(process_admin_selection, pass_user_data=True)],
                    WHITELIST_OPTION: [CallbackQueryHandler(whitelist_option)],
                    PAY_OPTION: [CallbackQueryHandler(pay_option)],
                    ENABLE_DISABLE_OPTION: [CallbackQueryHandler(trunkss_option, pass_user_data=True)],
                    WHITELIST_UPLOAD: [MessageHandler(filters=Filters.document, callback=whitelist_upload, pass_user_data=True)],
                    BOUNCEFILTER_OPTION: [CallbackQueryHandler(bouncefilter_option)],

                },
            )
            return conversation_handler

#WHITE_LIST = "whitelist"
#BOUNCE_FILTER = "bounce_filter"
#TRUNKS = "trunks"

        self.dispatcher.add_handler(CommandHandler('start', filters=self.private_filter, callback=show_help))
        self.dispatcher.add_handler(CommandHandler('debug', filters=self.private_filter, callback=debugCall))
        self.dispatcher.add_handler(build_conversation_handler())
        self.dispatcher.add_error_handler(handle_error)

    def start_bot(self):
        self.updater.start_polling()




def getTrunksName():
   try: 
      cnx2 = MySQLdb.connect(user=userdb,passwd=passdb,host=hostdb,db=dbasterisk)
      cursor2 = cnx2.cursor()
      cursor2.execute("SELECT name, disabled from trunks")
      trunks = cursor2.fetchall()
      cursor2.close()
      cnx2.close()
      return trunks
   except TypeError as e:
      log("{}: Falló getTrunksName".format(e))
      log("SELECT name, disabled from trunks")
      cursor2.close()
      cnx2.close()
   return trunks 

def getTrunksStatus():
                    statusOut = run(['/usr/sbin/asterisk', '-rx sip show peers '], stdout=PIPE).stdout.decode('utf-8')
                    salida="Name/username   Status\n"
                    for trunk,disabled in getTrunksName():
                        print(trunk,disabled)
                        if disabled=="on":
                            salida += "{} is disabled\n".format(trunk)
                        else:
                           for lin in statusOut.split('\n'):
                              if( match('^{}/.+'.format(trunk),lin) ):
                                 col = lin.split()
                                 try:
                                     salida += "{} {} {} {}\n".format(col[0], col[6], col[7], col[8])
                                 except IndexError as e:
                                     print(e)
                                     salida += "{}   {}\n".format(col[0], col[6])
                                     print(col)
                                    # "Online trunks: 0"

                    return salida


import os
def findAudio(name, path):
   for root, dirs, files in os.walk(path):
      if name in files:
         return os.path.join(root, name)

def loadWhitelist(file):
   with open(file, 'r') as fil:
      # Delete the whitelist set before loading the 
      # new file list
      print( "Deleting the whitelist if it is already loaded...: {}".format( r.delete("blanquito") ) )
      while True:
         num=fil.readline().strip('\n')
         num = num.replace('\r','')
         if(num==''):
            break
         r.sadd("blanquito", num)

