from scipy import signal
import numpy as np
#import matplotlib.pyplot as plt
from sys import argv
from scipy.io import wavfile
from util.config import SILENCE_THRESHOLD
# Be careful to give the parameter SILENCE_THRESHOLD before calling this method 
# TODO: It's time to write a Class called AudioAnalysis. This should be the 
# is_silence() method.

def silence_detected(audioSample):
    """ Square the signal, low pass filters it and check for a threshold. If its lower than
    that, then returns True. """ 
    try:
        fs, v  = wavfile.read(audioSample)
    except FileNotFoundError:
        print('not found', audioSample)
        return False
    if(len(v) != 0):
        # int64 because of overflowing when squaring, then appears negative numbers
        vsq   = np.array(v, dtype='int64')**2
        t=np.linspace(0, len(v)/fs, num=len(v))
        # Now, the order of the filter is 10 and the cut-of freq is at 10Hz:
        # Second Order 
        sos = signal.butter(10, 10, 'low', fs=fs, analog=False, output='sos')
        filtered = signal.sosfilt(sos,vsq )
        print('Silence analysis: {}. Threshold: {}'.format(max(filtered), SILENCE_THRESHOLD))
        #plt.figure()
        return max(filtered) < SILENCE_THRESHOLD 
        #plt.plot(t, filtered)
    #plt.plot(t, vsq)
    #plt.tight_layout()
   
if __name__ == "__main__":
    for audioSample in argv[1:]:
        if silence_detected(audioSample):
            print("Found silence on {}".format(audioSample))
    #plt.savefig('all.png' , facecolor='none', edgecolor='none', transparent=True, frameon=False)
    #plt.show()


