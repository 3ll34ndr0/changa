import os
import redis

CONFIG = {
   'gateways': {
             'host': {'var': 'GATEWAYS_REDIS_HOST', 'def': 'localhost'},
             'name': 'gateways',
             'params': {
             'component': 'redis',
             'user': '',
             'pass': '',
             }
             }
    }

#r = redis.Redis(host='localhost')
#   if (r.setnx(extension,1)):
#      r.expire(extension,28800)
#       r.incr(extension)


def connect(target=None, host=None):
    """ Connect to Redis database """
    if target is None:
        if host is None:
            host = get_redis_host()
    elif target in ['gateways', 'admin', 'filters', 'tariff']:
        if host is None and 'host' in CONFIG[target]:
            host = os.getenv(CONFIG[target]['host']['var'],
                             default=CONFIG[target]['host']['def'])
    else:
        raise RuntimeError('Invalid target: {}'.format(target))
    # TODO: user, password and port should/could be setted also
    return redis.Redis(host=host)



def get_redis_host():
    """ Get mysql host 3 """
    if 'REDIS_HOST_OVERRIDE' in os.environ:
        host = os.environ['REDIS_HOST_OVERRIDE']
    else:
        host = 'localhost'
    return host

def connect_gateways():
    return connect(target='gateways')


def sum_all_values(pattern, conn=None):
    """ Sum all values from keys with pattern <pattern> """
    if conn is None:
        conn = connect_gateways()
    counters = conn.keys(pattern=pattern)
    gw_counter = 0
    for c in counters:
        gw_counter += int(conn.get(c))
    return gw_counter


