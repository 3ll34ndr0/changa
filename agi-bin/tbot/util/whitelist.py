import os
import redis
r = redis.Redis(host='localhost')

def check_whitelist(extension):
   # Hardcoding whitelist set name should be avoided
   return r.sismember("blanquito", extension)

# This piece of code should be loaded in another library
def load_whitelist(filename):
   print('entra al load_whitelist')
   if os.path.exists(filename):
      print('parece que esiste')
   with open(filename, "r") as fil:
      print( "Deleting the whitelist if it is already loaded...:")
      # Delete the whitelist set before loading the 
      r.delete("blanquito")
      while True:
         num=fil.readline().strip("\n")
         num = num.replace('\r','')
         if(num==''):
            break
         r.sadd("blanquito", num)
         print(num)

