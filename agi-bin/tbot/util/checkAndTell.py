#!/usr/bin/env python3

from time import sleep
from datetime import datetime
from sys import argv
from util.pianito import pianito
from util.silence import silence_detected
from email.mime.text import MIMEText
from subprocess import Popen, PIPE, run
from re import match
from os import path
from redis import Redis
import logging
SILENCE_TAG = 4

try:
    import pymysql as MySQLdb
    MySQLdb.install_as_MySQLdb()
except ImportError:
    # MySQLdb is compatible with Python 2 only.  Try it as a
    # fallback if pymysql is unavailable.
    import MySQLdb


log_file = open('/var/log/asterisk/changarq.log', 'a', buffering=1)
def log(*args):
    print(datetime.now().isoformat()[:19], *args, file=log_file)
    print(datetime.now().isoformat()[:19], *args)

c_dir = path.dirname(__file__)
with open(path.join(c_dir, "../config/asterisksecrets.txt")) as key_file:
   hostdb, userdb, passdb, dbcdr, dbasterisk, monitor_dir = key_file.read().splitlines()

def checkAndTell(hostdb=hostdb,
               userdb=userdb, 
               passdb=passdb, 
               dbcdr=dbcdr,
               dbasterisk=dbasterisk,
               monitor_dir=monitor_dir):
   # Initial state
   needReloadAsterisk = False
   message=""
   cnx = MySQLdb.connect(user=userdb,passwd=passdb,host=hostdb,db=dbcdr)
   cursor = cnx.cursor()
   # uniqueid, trunkname, filename:
   sqlCommand = '''SELECT cdr.uniqueid, dstchannel, appdata, disposition FROM cdr INNER JOIN cel ON cdr.uniqueid = cel.uniqueid WHERE billsec = 0 AND (duration>12 AND duration<40 ) AND NOT checked AND calldate > (now() - INTERVAL 60 MINUTE) AND eventtype='APP_END' ORDER BY calldate LIMIT 1000'''
   cursor.execute(sqlCommand) 
   rowUniqueid = cursor.fetchall()
#   print(rowUniqueid)
   for uniqueid, trunkname, filename, disposition in rowUniqueid:
      trunkname=trunkname.split('/')[1].split('-')[0] #Quick and Dirty
      filename=monitor_dir+filename.split(",ai(")[0]
#      if filename.find(',') == -1:
#          # Asterisk won't save the file with ',', so we should avoid look for it
#          cursor.execute("UPDATE cdr set checked='1' WHERE uniqueid='{}'".format(uniqueid)) # 1: means true 0: false
##          cursor.execute("COMMIT") 
#          cursor.close()
#          cnx.close()

      cursor.execute("UPDATE cdr set checked='1' WHERE uniqueid='{}'".format(uniqueid)) # 1: means true 0: false
      if silence_detected(filename):
          print('Silencio en {}'.format(filename))
          found = (SILENCE_TAG, 0)
          m=''
          for i in rowUniqueid:
              m += '({} {})  '.format(i[1],i[3])
          print(m)

      else:
          found=pianito(filename)
#      if silence_detected(filename) and detected_twice_or_more(trunkname) and (disposition == 'BUSY' or disposition == 'FAILED'):
#            mes='Found Silence on trunk {}. Uniqueid: {} '.format(trunkname, uniqueid)
#            log(mes)
#            print(mes)
#            needReloadAsterisk = False
#            message+=mes+"\n"
#            return message

      if found and (disposition == 'BUSY' or disposition == 'FAILED'):
      #not (disposition == 'NO ANSWER' or disposition == 'ANSWERED'):
        if found[0] >= 0:
              needReloadAsterisk = True
              print('notified', uniqueid, found[0])
              log('notified', uniqueid, found[0])
              if(found[0]==1):
                 mes="Found blocked sim on trunk {} with uniqueid {}".format(trunkname, uniqueid)
                 log(mes)                  
                 print(mes)
                 message+=mes+"\n"
              elif(found[0]==2):
                 mes='Found Movistar SIM without credit on trunk {}. Uniqueid: {}'.format(trunkname, uniqueid)
                 log(mes)
                 print(mes)
                 message+=mes+"\n"
              elif(found[0]==3):
                 mes='Found Tuenti SIM without credit on trunk {}. Uniqueid: {}'.format(trunkname, uniqueid)
                 log(mes)
                 print(mes)
                 message+=mes+"\n"
                               #Now disable the trunk from asterisk db in trunks table:
              elif(found[0]==SILENCE_TAG):
                 mes='Found Silence on trunk {}. Uniqueid: {}.'.format(trunkname, uniqueid)
                 # Test mode
                 log(mes)
                 print(mes)
                 message+=mes+"\n"
                 cursor.execute("COMMIT") 
                 cursor.close()
                 cnx.close()
                 return message
              try: 
                 cnx2 = MySQLdb.connect(user=userdb,passwd=passdb,host=hostdb,db=dbasterisk)
                 cursor2 = cnx2.cursor()
                 sql_command = "UPDATE trunks set disabled='on' WHERE channelid='{}'".format(trunkname)
                 log(sql_command)
                 cursor2.execute(sql_command) 
              except TypeError as e:
                 log("{}: Fallo al intentar desabilitar el canal {}".format(e,trunkname))
<<<<<<< HEAD
=======
=======
>>>>>>> 049fb7b4f8dd94e72d8ee24d55a324150b337e2c
                 log("UPDATE trunks set disabled='on' WHERE name='{}'".format(trunkname))
                 cursor2.close()
                 cnx2.close()

        else:
           if (disposition == 'NO ANSWER' or disposition == 'ANSWERED'):
              print('Supuestamente no tiene que bloquearlo porque tiene un {}'.format(disposition))

           print('skiped', uniqueid, found, trunkname)
           log('skiped', uniqueid, found)
   # End for
   if(needReloadAsterisk):
      #asterisk
      cursor2.execute("COMMIT") 
      cursor2.close()
      cnx2.close()
      log('Now we are reloading asterisk ...')
      log(run(['/var/lib/asterisk/bin/module_admin', 'reload'], stdout=PIPE).stdout.decode('utf-8'))
   #asteriskcdrdb
   cursor.execute("COMMIT") 
   cursor.close()
   cnx.close()
   return message

<<<<<<< HEAD
=======

>>>>>>> 049fb7b4f8dd94e72d8ee24d55a324150b337e2c
def detected_twice_or_more(trunkName):
   r = Redis(host='localhost')
   logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                             level=logging.INFO)

   if (r.setnx(trunkName,1)):
      # Expires in two minutes
      r.expire(trunkName,120)
      log("In trunk {} a silence has been found for the first time".format(trunkName) )
      # 1: puede llamar
      detectedTwice = False
      
   else: 
      r.incr(trunkName)
      log("In trunk {} silence has been find {} times.".format(trunkName, str(r.get(trunkName))) )
      # Un cero, paraque cuelgue:
      detectedTwice = True

   return detectedTwice



if __name__ == '__main__':
   #Useful to debug
   c_dir = path.dirname(__file__)
   with open(path.join(c_dir, "config/asterisksecrets.txt")) as key_file:
      hostdb, userdb, passdb, dbcdr, dbasterisk, monitor_dir = key_file.read().splitlines()
   checkAndTell (hostdb, userdb, passdb, dbcdr, dbasterisk, monitor_dir)

