from bs4 import BeautifulSoup
import urllib3
import re

class Gateway(object):
    def __init__(self, name):
        self.name        = name
        self.channels    = dict()
        self.av_channels = set()
        # Rise an alarm if the GW is not reachable
        self.offline     = False
        # This are the blocked channels. May not be needed.
        self.blocked     = dict()


    def get(self,channel=None):
        """ Returns a dictionary with the asked data. TODO: Formated string instead?"""
        if channel:
            return self.channels[channel]
        else:
            return self.channels
#    def create(self, host, port='80', user='admin', password='admin')
       


class Goip (Gateway):
    """
    Example: gw = Goip("http://your-gateway-public-ip:89", "JustAName")
    Will try to login to your gateway web administration using admin:admin credentials"
    """
    def __init__(self, url, name, user="admin", password="admin"):
        super().__init__(name)
        self.url         = url+"/default/en_US/status.html"
        self.http        = urllib3.PoolManager()
        self.headers     = urllib3.util.make_headers(basic_auth=user+":"+password)

    def update(self):
        # TODO: Instate of use enumerate, parse the 'Line' value
        self.soup = BeautifulSoup(self.http.request('GET', self.url, headers=self.headers).data , 'html.parser')
        for j,i in enumerate(self.soup.find_all(id=self._has_sim), 1):
            self.channels[j]= dict()
            self.channels[j]['sim'] = i.text
        for j,i in enumerate(self.soup.find_all(id=self._has_carrier),1):
            self.channels[j]['Carrier'] = i.text #next(i.stripped_strings)
        for j,i in enumerate(self.soup.find_all(id=self._has_gsm),1):
            self.channels[j]['gsm'] = i.text
        for j,i in enumerate(self.soup.find_all(id=self._has_voip),1):
            self.channels[j]['voip'] = i.text
        for j,i in enumerate(self.soup.find_all(id=self._has_status),1):
            self.channels[j]['Status'] = next(i.stripped_strings)
        for j,i in enumerate(self.soup.find_all(id=self._has_duration),1):
            self.channels[j]['Duration'] = next(i.stripped_strings)
            if (self.channels[j]['Status'] == 'IDLE') and (self.channels[j]['voip'] == 'Y') and (self.channels[j]['gsm'] == 'Y') and (self.channels[j]['sim'] == 'Y'):
                self.av_channels.add(str(j))

    def _has_sim(self, id):
        return id and re.compile("gsm_sim$").search(id)

    def _has_gsm(self, id):
        return id and re.compile("gsm_status").search(id)

    def _has_voip(self, id):
        return id and re.compile("status_line").search(id)

    def _has_status(self, id):
        return id and re.compile("line_state").search(id)

    def _has_carrier(self, id):
        return id and re.compile("_gsm_cur_oper").search(id)

    def _has_duration(self, id):
        return id and re.compile("_callt").search(id)
