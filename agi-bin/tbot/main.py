import logging
from os import path
from core.telegrambot import TelegramBot
from util.checkAndTell import checkAndTell 

if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)

    c_dir = path.dirname(__file__)
    with open(path.join(c_dir, "config/secrets.txt")) as key_file:
        #chat_id is the channel id where the bot is publishing the ivr results
        api_key, secret, telegram_tkn, user_id, chat_id = key_file.read().splitlines()

    telegram_bot = TelegramBot(telegram_tkn, user_id, chat_id, checkAndTell)
    telegram_bot.start_bot()

