#!/usr/bin/env python3

from sys import argv
import redis
import logging
from datetime import datetime
from whitelist import checkWhitelist #loadWhitelist



log_file = open('/var/log/asterisk/solouna.log', 'a', buffering=1)
def log(*args):
   print(datetime.now().isoformat()[:19], *args, file=log_file)
   #print(datetime.now().isoformat()[:19], *args)

def solouna(extension, uniqueid):
   r = redis.Redis(host='localhost')
   logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                             level=logging.INFO)

   if (r.setnx(extension,1)):
      r.expire(extension,14400)
      #Four hours
      log("{}, 1, {}".format(extension, uniqueid))
      # 1: puede llamar
      status="1"
      
   else: 
      r.incr(extension)
      log("{}, {}, {}".format(extension, str(r.get(extension)), uniqueid))
      # Un cero, paraque cuelgue:
      status="0"

   return status
 

if __name__ == '__main__':
   extension, uniqueid =  argv[1:]
   print(solouna(extension, uniqueid))

