#!/usr/bin/env python3

import redis
r = redis.Redis(host='localhost')
tarif='tarif_prueba'
def checkWhitelist(extension):
   # Hardcoding whitelist set name should be avoided
   return r.sismember(tarif, extension)

def loadWhitelist(file):
#   fil = open(file, "r")
   with open(file, "r") as fil:
      # Delete the whitelist set before loading the 
      # new file list
      print( "Deleting the whitelist if it is already loaded...: {}".format( r.delete(tarif) ) )
      while True:
         num=fil.readline().strip("\n")
         if(num==''):
            break
         r.sadd(tarif, num)
         #print(num)

