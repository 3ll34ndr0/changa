#!/usr/bin/env php
<?php
//	License for all code of this FreePBX module can be found in the license file inside the module directory
//	Copyright 2017 Sangoma Technologies Inc

//Bootstrap FreePBX
$bootstrap_settings['freepbx_auth'] = false;
require_once "phpagi.php";
$agi = new AGI();
// avoid this hardcoding1
$db = 'asterisk';
// avoid this also:
$agi_bin_dir = '/var/lib/asterisk/agi-bin';
$hostdb   = get_var( $agi, "AMPDBHOST");
$passdb   = get_var( $agi, "AMPDBPASS");
$userdb   = get_var( $agi, "AMPDBUSER");
$uniqueid = get_var( $agi, "UNIQUEID");
$extension = argv[1];
mi_verbose( $agi, $extension);
$command = $agi_bin_dir.'/'.'amiable.py '.$hostdb.' '.$userdb.' '.$passdb.' '.$db.' '.$uniqueid.' '.$extension;
mi_verbose( $agi, $command);
exec($command, $out, $status);
mi_verbose( $agi, 'Esto es lo que sale de esta mierda: '.$out[0]);



// No trunks availables then we must send 503 sip code (hangup(34))
if ($out[0] == "0") {
	//Lo meto para hacer 503
	$agi->hangup(34);
	$currentChannel= $agi->request['agi_channel'];
	$cuelgue=$agi->hangup($currentChannel);
        $agi->set_variable('CUELGUE', '1');
}
//
function get_var( $agi, $value) {
	$r = $agi->get_variable( $value );

	if ($r['result'] == 1) {
		$result = $r['data'];
		return $result;
	}
	return '';
}
function mi_verbose($agi, $string, $level=1) {
	$agi->verbose($string, $level);
}
