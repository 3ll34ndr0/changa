#!/usr/bin/env php
<?php

//Bootstrap FreePBX
$bootstrap_settings['freepbx_auth'] = false;
require_once "phpagi.php";
$agi = new AGI();
// avoid this hardcoding1
// avoid this also:
$agi_bin_dir = '/var/lib/asterisk/agi-bin';
//$hostdb   = get_var( $agi, "AMPDBHOST");
//$passdb   = get_var( $agi, "AMPDBPASS");
//$userdb   = get_var( $agi, "AMPDBUSER");
$uniqueid = get_var( $agi, "UNIQUEID");
$extension= $agi->request['agi_extension'];
$command = $agi_bin_dir.'/'.'solouna.py '.$extension.' '.$uniqueid;
mi_verbose( $agi, $command);
exec($command, $out, $status);
mi_verbose( $agi, 'E s t o e s l o q u e s a l e     d  e e  s  t  a m  i  er  d  a: '.$out[0]);

if ($out[0] == "0") {
	//Lo meto para hacer 503
	$agi->hangup(34);
	$currentChannel= $agi->request['agi_channel'];
	$cuelgue=$agi->hangup($currentChannel);
        $agi->set_variable('CUELGUE', '1');
}

function get_var( $agi, $value) {
	$r = $agi->get_variable( $value );

	if ($r['result'] == 1) {
		$result = $r['data'];
		return $result;
	}
	return '';
}
function mi_verbose($agi, $string, $level=1) {
	$agi->verbose($string, $level);
}
