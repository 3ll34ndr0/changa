# changa

## FreePBX customization for my own needs ##

This was my first try in the VoIP industry. I had to deploy a system that could get the calls from a SIP trunk originator and route them to several GOIP gsm gateways. It can also be used with any gateway, as far as you configure them to register in this box.

Freepbx only gave me the call recording interface, as well as the CDR module. The main interface for this system is a telegram bot. Below you can see some screenshots. 

## Screenshots

### Admin Interface
![Image Admin Interface](/figs/adminOptions.jpg)


### Trunk Status
![Image Trunk Status](/figs/trunksStatus.jpg)

### Active Calls
![Image Trunk Status](/figs/activeCalls.jpg)


## Disclaimer
This project is by no means a finished work, and chances are that you can't use it if you are not a developer. And if you are a developer who can understand this, you may only care about the Telegram Bot, everything else is low quality code. It is a quick and dirty development for a real world business, with fast to deliver goal and very low bugdet. Full of hardcoded configurations and bad practices examples. I had to erase some code that doesn't belongs to me. But if you want the
    same functionality, I can use a free software project to replace it. It will only take some days.
